provider "aws" {}

variable vpc_cidr_block {}
variable subnet_cidr_block {}
variable avail_zone {}
variable env_prefix {}
variable my_ip {}
variable instance_type {}
variable my_sshkey_location {}

resource "aws_vpc" "myapp_vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name: "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "myapp_subnet1" {
  vpc_id = aws_vpc.myapp_vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.avail_zone
  tags = {
    Name: "${var.env_prefix}-subnet1"
  }
}

/*resource "aws_route_table" "myapp_rtb" {
  vpc_id = aws_vpc.myapp_vpc.id
  
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp_igw.id
  }
  tags = {
    Name: "${var.env_prefix}-rtb"
  }
}*/

resource "aws_internet_gateway" "myapp_igw" {
  vpc_id = aws_vpc.myapp_vpc.id
  tags = {
    Name: "${var.env_prefix}-igw"
  }
}

/*resource "aws_route_table_association" "asso_rtb_subnet" {
  subnet_id = aws_subnet.myapp_subnet1.id
  route_table_id = aws_route_table.myapp_rtb.id
}*/

resource "aws_default_route_table" "main-rtb" {
  default_route_table_id = aws_vpc.myapp_vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp_igw.id
  }
  tags = {
    Name: "${var.env_prefix}-rtb"
  }
}

resource "aws_default_security_group" "default-sg" {
  #name = "myapp-sg"
  vpc_id = aws_vpc.myapp_vpc.id

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name: "${var.env_prefix}-sg"
  }
}

data "aws_ami" "latest_amazon_image" {
  most_recent = true
  owners = ["137112412989"]
  filter {
    name = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-x86_64-gp2"]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}

/*output "aws_ami_id" {
  value = data.aws_ami.latest_amazon_image.id
}*/

resource "aws_key_pair" "ssh-key" {
  key_name = "server-key-pair"
  public_key = file(var.my_sshkey_location)
}

resource "aws_instance" "myserver" {
  ami = data.aws_ami.latest_amazon_image.id
  instance_type = var.instance_type

  subnet_id = aws_subnet.myapp_subnet1.id
  vpc_security_group_ids = [aws_default_security_group.default-sg.id]
  availability_zone = var.avail_zone

  associate_public_ip_address = true
  key_name = aws_key_pair.ssh-key.key_name

  /*user_data = <<EOF
                  #!/bin/bash
                  sudo yum update -y
                  sudo yum install -y docker
                  sudo systemctl enable docker
                  sudo systemctl start docker
                  sudo usermod -aG docker ec2-user
                  docker run -p 8080:80 nginx
              EOF*/

  user_data = file("entry-script.sh")
  
  tags = {
    Name: "${var.env_prefix}-server"
  }
}

output "instance_public_ip" {
  value = aws_instance.myserver.public_ip
}